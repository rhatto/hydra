#
# Makefile for The Hydra Suite
#

.PHONY: publish docs

web_deploy:
	@rsync -avz --delete site/   hydra:/var/sites/hidra/www/
	@rsync -avz --delete slides/ hydra:/var/sites/hidra/www/slides/

docs:
	@mkdocs build

publish: docs web_deploy
