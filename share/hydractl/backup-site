#!/bin/bash
#
# Backup a site.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Load.
source $APP_BASE/lib/hydra/functions || exit 1
hydra_config_load

# Basic parameters.
SITES="/var/sites"
SITE="$1"
PACK="$1.tar.bz2"
DATE="`date +%Y%m%d`"
DEST_BASE="$SITES/backups/site"
DEST_BASE_SITE="$DEST_BASE/$SITE"
DEST="$DEST_BASE_SITE/`facter hostname`/$DATE"
DATABASE="`hydra_database_name $SITE`"

# Syntax check.
if [ -z "$SITE" ]; then
  hydra_action_usage
  exit 1
fi

# Determine site location.
if [ "$SITE" == "git" ] || [ "$SITE" == "svn" ]; then
  LOCATION="/var/$SITE"
elif [ "$SITE" == "debian" ]; then
  LOCATION="/var/reprepro"
elif [ "$SITE" == "wiki" ]; then
  LOCATION="/var/www/data/`ls -1 /var/www/data | grep pmwiki | sort -n | head`"

  if [ "$LOCATION" == "/var/www/data" ]; then
    echo "PmWiki installation not found at /var/www/data."
    exit 1
  fi
else
  LOCATION="$SITES/$SITE"
fi

# Set backups user.
if hydra_check_user backups; then
  BACKUPS_USER="backups"
else
  BACKUPS_USER="root"
fi

# Set backups group.
if hydra_check_group backups; then
  BACKUPS_GROUP="backups"
else
  BACKUPS_GROUP="root"
fi

# Check destination base index
mkdir -p $DEST_BASE
touch $DEST_BASE/index.html

# Fix old paths
rm -f $DEST_BASE/../index.html
rm -f $DEST_BASE/../robots.txt

# Check robots.txt
if [ ! -e "$DEST_BASE/site/robots.txt" ]; then
  echo 'User-agent: *' > $DEST_BASE/robots.txt
  echo 'Disallow: /'  >> $DEST_BASE/robots.txt
fi

# Create folder
mkdir -p $DEST
cd $DEST

# Password setup
if [ ! -e "$DEST_BASE_SITE/.htpasswd" ]; then
  touch $DEST_BASE_SITE/.htpasswd
  chmod 640 $DEST_BASE_SITE/.htpasswd
  chown root:$BACKUPS_GROUP $DEST_BASE_SITE/.htpasswd
fi

# Access setup
if [ ! -e "$DEST_BASE_SITE/.htaccess" ]; then
  cat > $DEST_BASE_SITE/.htaccess <<-EOF
AuthType Basic
AuthName "Backup $SITE"
AuthUserFile $DEST_BASE_SITE/.htpasswd
Require valid-user
EOF
fi

# Backup site
if [ -d "$LOCATION" ]; then
  if [ -e "$PACK" ]; then
    echo "File $DEST/$PACK already exists, skipping..."
  else
    echo "Backing up site folder..."
    tar jcvf $PACK $LOCATION
    md5sum $PACK  > $PACK.md5
    sha1sum $PACK > $PACK.sha1
    chown root:$BACKUPS_GROUP $PACK*
    chmod 640 $PACK*
    echo "Saved $DEST/$PACK"
  fi
fi

# Backup database
if [ -d "/var/lib/mysql/$DATABASE" ]; then
  if [ -e "$DATABASE" ]; then
    echo "File $DATABASE already exists, skipping..."
  else
    echo "Backing up database $DATABASE..."
    mysqldump $DATABASE > $DATABASE.sql
    bzip2 $DATABASE.sql
    rm -f $DATABASE.sql
    md5sum $DATABASE.sql.bz2 > $DATABASE.sql.bz2.md5
    sha1sum $DATABASE.sql.bz2 > $DATABASE.sql.bz2.sha1
    chown root:$BACKUPS_GROUP $DATABASE.sql*
    chmod 640 $DATABASE.sql*
    echo "Saved $DEST/$DATABASE.sql.bz2"
  fi
fi
