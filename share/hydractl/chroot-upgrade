#!/bin/bash
#
# Chroot mass upgrader.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Basic parameters
BASE="/var/chroot/"

# Set sudo config
if [ "`whoami`" != 'root' ]; then
  sudo="sudo"
fi

# Check base
if [ ! -d "$BASE" ]; then
  exit
fi

# Upgrade all chroots
for chroot in `ls $BASE`; do
  folder="$BASE/$chroot"
  if [ -f "$folder/etc/debian_version" ]; then
    echo "Upgrading $folder..."
    $sudo mount none -t proc $folder/proc
    $sudo mount -o bind /dev $folder/dev
    $sudo mount -o bind /var/cache/apt $folder/var/cache/apt
    $sudo mount -t devpts devpts $folder/dev/pts
    $sudo cp /etc/resolv.conf $folder/etc
    $sudo chroot $folder apt-get update
    $sudo chroot $folder apt-get dist-upgrade -y
    $sudo chroot $folder apt-get autoremove -y
    $sudo umount $folder/proc
    $sudo umount $folder/dev/pts
    $sudo umount $folder/dev
    $sudo umount $folder/var/cache/apt
  fi
done
