#!/bin/bash
#
# System installer, chroot version.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Load.
source $APP_BASE/lib/hydra/functions || exit 1
hydra_config_load

# Make sure there is provision config.
function hydra_provision_config {
  hydra_user_config   folder            /var/chroot/debian               "Destination device"
  hydra_user_config   hostname          machine                          "Hostname"
  hydra_user_config   domain            example.org                      "Domain"
  hydra_user_config   arch              amd64                            "System arch"
  hydra_user_config   version           stretch                          "Distro version"
  hydra_user_config   mirror            https://deb.debian.org/debian/   "Debian mirror"
}

# Load configuration
hydra_provision_config_load $1

# Get config parameters.
hydra_provision_config

# Parameters
WORK="$folder"
CHROOT="hydra_sudo_run chroot $WORK"

# Check for requirements.
for req in debootstrap qemu-user-static; do
  hydra_install_package $req
done

hydra_sudo_run mkdir -p /var/chroot
hydra_sudo_run debootstrap --force-check-gpg --variant=minbase --arch $arch $version $WORK $mirror

# Apt
if [ "$version" != "sid" ]; then
  echo "deb http://security.debian.org/debian-security $version-security main contrib non-free"     | $SUDO tee $WORK/etc/apt/sources.list
  echo "deb-src http://security.debian.org/debian-security $version-security main contrib non-free" | $SUDO tee $WORK/etc/apt/sources.list
fi

# Initial upgrade.
echo "Applying initial upgrades..."
hydra_sudo_run chroot $WORK/ apt-get update
hydra_sudo_run chroot $WORK/ apt-get upgrade -y

# Arch specific procedures
if [ "$arch" == "armel" ] || [ "$arch" == "armhf" ]; then
  if [ ! -f '/usr/bin/qemu-arm-static' ]; then
    hydra_sudo run apt-get install qemu-user-static
  fi

  hydra_sudo_run cp /usr/bin/qemu-arm-static $WORK/usr/bin/
  $CHROOT /debootstrap/debootstrap --second-stage
fi

echo "$hostname.$domain" | hydra_sudo_run tee $WORK/etc/hostname > /dev/null
$CHROOT apt-get install -y locales
