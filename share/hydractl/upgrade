#!/bin/bash
#
# Run a system upgrade.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Basic parameters
CLEAN="$1"

# Set sudo config
if [ "`whoami`" != 'root' ]; then
  sudo="sudo"
fi

# Issue upgrade
if ! $sudo lsof /var/lib/dpkg/lock &> /dev/null; then
  # Fix any pending upgrades
  $sudo dpkg --configure -a

  # Proceed to the actual upgrade
  $sudo apt-get update
  $sudo apt-get dist-upgrade -y
  $sudo apt-get autoremove -y
else
  echo "Apt is locked, aborting."
fi

# Upgrade raspberry
# Do not run it under normal circunstances
# https://github.com/Hexxeh/rpi-update
#if [ -x "/usr/bin/rpi-update" ]; then
#  rpi-update
#fi

# Upgrade all chroots
hydractl chroot-upgrade

# Upgrade all pbuilder chroots
hydractl pbuilder-upgrade

# Upgrade all flatpak packages
if which flatpak &> /dev/null; then
  $sudo flatpak update -y
fi

# Upgrade all snap packages
if which snap &> /dev/null; then
  $sudo snap refresh
fi

# Cleanup
if [ "$CLEAN" != "noclean" ]; then
  $sudo apt-get clean
fi
