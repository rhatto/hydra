#!/bin/bash
#
# Import keys into nodes.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Load
source $APP_BASE/lib/hydra/functions || exit 1
hydra_config_load

# Import OpenPGP keypair
# See https://superuser.com/questions/1135812/gpg2-asking-for-passphrase-when-importing-secret-keys#1135950
#     https://dev.gnupg.org/T2313
function hydra_import_keys_openpgp {
  key="$(keyringer $HYDRA decrypt nodes/$hostname/gpg/key 2> /dev/null | sed -ne '1,$p')"
  pubkey="$(keyringer $HYDRA decrypt nodes/$hostname/gpg/key.pub 2> /dev/null | sed -ne '1,$p')"
  passphrase="$(keyringer $HYDRA decrypt nodes/$hostname/gpg/key.passwd 2> /dev/null)"
  key_id="$(echo "$pubkey" | gpg --with-colons | grep pub | cut -d : -f 5)"

  if [ -z "$key" ]; then
    echo "Could not find key for $node, skipping."
    continue
  fi

  if [ "`facter fqdn`" != "$hostname" ]; then
    $HYDRA_CONNECT $hostname <<EOF
    ##### BEGIN REMOTE SCRIPT #####
    echo ""
    echo "-----------------------------"
    echo "Importing gpg key to $node..."
    echo "-----------------------------"
    echo ""
    echo -e "${passphrase}\n${key}" | sudo gpg --passphrase-fd 0 --batch --no-tty --homedir /root/.gnupg --import

    echo ""
    echo "Trusting key $key_id at $node..."
    echo ""
    printf "trust\n5\ny\nsave\n" | sudo gpg --homedir /root/.gnupg --no-tty --status-fd=2 --command-fd=0 --edit-key $key_id

    echo ""
    echo "Verifying..."
    echo ""
    sudo gpg --homedir /root/.gnupg --list-keys
    ##### END REMOTE SCRIPT #######
EOF
  else
    echo ""
    echo "-----------------------------"
    echo "Importing gpg key to $node..."
    echo "-----------------------------"
    echo ""
    echo -e "${passphrase}\n${key}" | sudo gpg --passphrase-fd 0 --no-tty --homedir /root/.gnupg --import

    echo ""
    echo "Trusting key $key_id at $node..."
    echo ""
    printf "trust\n5\ny\nsave\n" | sudo gpg --homedir /root/.gnupg --no-tty --status-fd=2 --command-fd=0 --edit-key $key_id

    echo ""
    echo "Verifying..."
    echo ""
    sudo gpg --homedir /root/.gnupg --list-keys
  fi
}

# Import OpenSSH keypair
function hydra_import_keys_openssh {
  if [ "`facter fqdn`" != "$hostname" ]; then
    echo "-----------------------------------------------------"
    echo "Importing keypair at $hostname:/root/.ssh..."
    echo "-----------------------------------------------------"

    echo "Creating folder structure at $hostname:/root/.ssh..."
    $HYDRA_CONNECT $hostname <<EOF
    sudo mkdir -p        /root/.ssh
    sudo chown root:root /root/.ssh
    sudo chmod 700       /root/.ssh
    sudo touch           /root/.ssh/id_rsa
    sudo touch           /root/.ssh/id_rsa.pub
    sudo chmod 600       /root/.ssh/id_rsa
    sudo chmod 600       /root/.ssh/id_rsa.pub
EOF

    echo "Importing public key from keyringer to $hostname:/root/.ssh..."
    keyringer $HYDRA decrypt nodes/$hostname/ssh/id_rsa.pub | \
      $HYDRA_CONNECT $hostname "cat - | sudo tee /root/.ssh/id_rsa.pub > /dev/null"

    echo "Importing private key from keyringer to $hostname:/root/.ssh..."
    keyringer $HYDRA decrypt nodes/$hostname/ssh/id_rsa | \
      $HYDRA_CONNECT $hostname "cat - | sudo tee /root/.ssh/id_rsa > /dev/null"
  else
    echo "-----------------------------------------------------"
    echo "Importing keypair at $hostname:/root/.ssh..."
    echo "-----------------------------------------------------"

    echo "Creating folder structure at $hostname:/root/.ssh..."
    sudo mkdir -p        /root/.ssh
    sudo chown root:root /root/.ssh
    sudo chmod 700       /root/.ssh
    sudo touch           /root/.ssh/id_rsa
    sudo touch           /root/.ssh/id_rsa.pub
    sudo chmod 600       /root/.ssh/id_rsa
    sudo chmod 600       /root/.ssh/id_rsa.pub

    echo "Importing public key from keyringer to $hostname:/root/.ssh..."
    keyringer $HYDRA decrypt nodes/$hostname/ssh/id_rsa.pub | sudo tee /root/.ssh/id_rsa.pub > /dev/null

    echo "Importing private key from keyringer to $hostname:/root/.ssh..."
    keyringer $HYDRA decrypt nodes/$hostname/ssh/id_rsa | sudo tee /root/.ssh/id_rsa > /dev/null
  fi
}

# Import Borg key
#
# Borg does not support using pre-generated keys anymore (as of 2024-05-16).
#
# This code is therefore deprecated, but will stay here for a while, as maybe
# in the long term borg starts to support this again.
#
# Check also https://github.com/borgbackup/borg/issues/7047
#            https://borgbackup.readthedocs.io/en/latest/faq.html#how-important-is-the-home-config-borg-directory
function hydra_import_keys_borg {
  if [ "`facter fqdn`" != "$hostname" ]; then
    echo "-----------------------------------------------------"
    echo "Importing borg key at $hostname:/root/.config/borg/hydra/key..."
    echo "-----------------------------------------------------"

    echo "Creating folder structure at $hostname:/root/.config/borg..."
    $HYDRA_CONNECT $hostname <<EOF
    sudo rm    -rf       /root/.borg
    sudo mkdir -p        /root/.config/borg/hydra
    sudo chown root:root /root/.config/borg
    sudo chown root:root /root/.config/borg/hydra
    sudo chmod 700       /root/.config/borg
    sudo chmod 700       /root/.config/borg/hydra
    sudo touch           /root/.config/borg/hydra/key
    sudo chmod 600       /root/.config/borg/hydra/key
EOF

    echo "Importing borg key from keyringer to $hostname:/root/.config/borg/hydra/key..."
    keyringer $HYDRA decrypt nodes/$hostname/borg/key | \
      $HYDRA_CONNECT $hostname "cat - | sudo tee /root/.config/borg/hydra/key > /dev/null"
  else
    echo "-----------------------------------------------------"
    echo "Importing borg key at $hostname:/root/.config/borg/hydra/key..."
    echo "-----------------------------------------------------"

    # Remove old, wrong borg config
    sudo rm -rf /root/.borg

    echo "Creating folder structure at $hostname:/root/.config/borg..."
    sudo mkdir -p        /root/.config/borg/hydra
    sudo chown root:root /root/.config/borg
    sudo chown root:root /root/.config/borg/hydra
    sudo chmod 700       /root/.config/borg
    sudo chmod 700       /root/.config/borg/hydra
    sudo touch           /root/.config/borg/hydra/key
    sudo chmod 600       /root/.config/borg/hydra/key

    echo "Importing borg key from keyringer to $hostname:/root/.config/borg/hydra/key..."
    keyringer $HYDRA decrypt nodes/$hostname/borg/key | sudo tee /root/.config/borg/hydra/key > /dev/null
  fi
}

# Command line arguments
NODES="$*"

# Build node list
if [ -z "$NODES" ]; then
  NODES="`hydra $HYDRA nodes`"
fi

# Deploy
for node in $NODES; do
  hostname="`hydra_get_fqdn_from_nodename $node`"

  # Import OpenPGP keypair
  hydra_import_keys_openpgp

  # Import OpenSSH keypair
  hydra_import_keys_openssh

  # Import Borg key
  hydra_import_keys_borg
done
