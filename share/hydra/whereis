#!/bin/bash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Load.
source $APP_BASE/lib/hydra/functions || exit 1
hydra_config_load

# Parameters
BASENAME="`basename $0`"
LAYER="$1"
DOMAIN="`hydra $HYDRA config domain`"

# Checks
if [ -z "$LAYER" ]; then
  echo "usage: $BASENAME <subdomain>"
  exit 1
elif [ -z "$DOMAIN" ]; then
  echo "Please set 'domain' at $HYDRA hydra config."
  exit 1
fi

# Puppet config
echo "Puppet config at $PUPPET"
echo "----------------------------------------------------------------------------------"
echo ""

# Is it a node?
if [ -e "$PUPPET/config/node/$LAYER.$DOMAIN.yaml" ]; then
  echo "Defined as a node at config/node/$LAYER.$DOMAIN.yaml"
  echo "Configuration:"
  echo ""
  grep "nodo::role:"     $PUPPET/config/node/$LAYER.$DOMAIN.yaml
  grep "nodo::location:" $PUPPET/config/node/$LAYER.$DOMAIN.yaml
fi

# DNS config
if [ -e "$HYDRA_FOLDER/dns/$DOMAIN.conf" ]; then
  echo ""
  echo "DNS configuration at dns/$DOMAIN.conf"
  echo "----------------------------------------------------------------------------------"
  echo ""

  RESPONSE="`grep -e "^$LAYER " $HYDRA_FOLDER/dns/$DOMAIN.conf`"

  # Turn off pathname expansion so expansion can work properly
  set -f
  if [ -z "$RESPONSE" ]; then
    RESPONSE="`grep -e '^* ' $HYDRA_FOLDER/dns/$DOMAIN.conf`"
  fi

  echo $RESPONSE
fi

# DNS
echo ""
echo "DNS response"
echo "----------------------------------------------------------------------------------"
echo ""
dig +noauthority +nocomments +nocmd +nostats +noadditional $LAYER.$DOMAIN

# GeoIP
if which geoiplookup &> /dev/null; then
  echo ""
  echo "GeoIP response"
  echo "----------------------------------------------------------------------------------"
  echo ""
  geoiplookup $LAYER.$DOMAIN
fi

# Service status
echo ""
echo "Ping response"
echo "----------------------------------------------------------------------------------"
echo ""
ping -q -n -c 1 $LAYER.$DOMAIN | tail -n 2
