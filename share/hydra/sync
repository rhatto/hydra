#!/bin/bash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Load.
source $APP_BASE/lib/hydra/functions || exit 1
hydra_config_load

# Sync each repository.
function hydra_sync {
  for repository in $*; do
    if [ -e "$HYDRA_FOLDER/config/repository/ignore/$repository" ]; then
      continue
    fi

    if [ "$repository" == "keyringer" ]; then
      UPDATE_KEYRINGER="yes"
      continue
    fi
  
    # Determine repository URL
    if [ ! -d "$HYDRA_FOLDER/$repository" ]; then
      if [ -e "$HYDRA_FOLDER/config/repository/url" ]; then
        url="`cat $HYDRA_FOLDER/config/repository/url`"
      else
        if [ ! -e "$HYDRA_FOLDER/config/domain" ]; then
          echo "Please set domain configuration"
          exit 1
        fi
        url="git@admin.`cat $HYDRA_FOLDER/config/domain`:$repository"
      fi

      git clone ${url}${repository} $HYDRA_FOLDER/$repository
    fi
  
    if [ -e "$HYDRA_FOLDER/$repository/.git" ]; then
      echo "Syncing $repository..."
  
      if [ -d "$HYDRA_FOLDER/$repository/.git/annex" ]; then
        # Check for requirements.
        for req in git-annex; do
          hydra_install_package $req
        done

        ( cd $HYDRA_FOLDER/$repository && git annex sync )
      else
        (
          cd $HYDRA_FOLDER/$repository
          if git remote | grep -q "origin"; then
            git pull origin master
            git submodule update --init
          else
            echo "No origin repository configured for $repository, skipping..."
          fi
        )
      fi
    fi
  done

  # Sync keyringer at last
  if [ "$UPDATE_KEYRINGER" == "yes" ]; then
    hydra_sync_keyringer
  fi
}

# Update keyring.
function hydra_sync_keyringer {
  if [ "$UPDATE_KEYRINGER" != "no" ]; then
    if [ ! -e "$HOME/.keyringer/$HYDRA" ]; then
      if [ -e "$HYDRA_FOLDER/keyring" ]; then
        if ! which keyringer &> /dev/null; then
          hydra_install_package keyringer
        fi

        echo "Initializing keyring for $HYDRA..."
        keyringer $HYDRA init $HYDRA_FOLDER/keyring

        echo "Syncing keyringer..."
        keyringer $HYDRA git pull
      fi
    fi
  fi
}

# Basic parameters.
if [ -z "$1" ]; then
  REPOSITORIES="$BASEREPOS"
else
  REPOSITORIES="$*"
fi

# Check if this hydra uses a superproject
if [ -d "$HYDRA_FOLDER/.git" ]; then
  # Update submodules
  ( cd $HYDRA_FOLDER && git submodule update --recursive )
else
  # Sync each repository
  hydra_sync $REPOSITORIES
fi
