# Management

## Sets and supersets

The Hydra Suite is flexible enough so you can use it to:

* Manage a single machine using `hydractl` command.
* Manage a set or a subset of machines -- a *network* -- from the same
  organization using `hydra` command.
* Manage a superset of machines -- *a constelation* -- comprised of one or more
  organizations using `hydras` command.

## Creating a new network

Use the following commands to create the needed repositories when starting a
new network:

    hydra example init   ~/file/example
    hydra example config domain example.org

This makes `~/files/example` the base folder for all configuration of the
network of machines called `example`.

## Managing an existing network

If you have enough credentials (ssh pubkey set in the server), you can fetch
all the needed repositories and configurations to a local folder
`~/file/example` using simply the following command:

    hydra example init ~/file/example git@admin.example.org:

This will try to fetch a superproject from git@admin.example.org:example.git
-- if exists, plus submodules -- or each git repository used by a network
previously created by the `init` action (`config.git`, `puppet.git`, etc).
