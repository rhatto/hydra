# TODO

## hydra

* [ ] Deploy:
    * [ ] Collect basic hardware information along with facts: cpuid,
          dmidecode, hwinfo, lsblk, lscpu, lshw, lspci, lsusb, sfdisk etc.
          Store somewhere under the `config/hardware` folder?
          Could also be a separate action, `hydra <hydra> collect <node>`.
    * [ ] Command line is broken for ansible when multiple nodes are provided.
    * [ ] Use console-based GnuPG agent when calling `keyringer`.
* [ ] Mass:
    * [ ] Support for [cumin][] ([Debian package](https://tracker.debian.org/pkg/cumin)).

[cumin]: https://github.com/wikimedia/cumin

## hydractl

* [ ] External volume script (replacing new drive procedure from
      `docs/backups.md`).
* [ ] Mount/umount media:
    * [ ] Fix STANDBY handling on external drives:
          > Disabling STANDBY on drive...
          > couldn't find field acronym: STANDBY
          >     [perhaps a '--transport=<tn>' or '--vendor=<vn>' option is needed]
    * [ ] Mount/umount system volume supporting split partiton scheme (`root`,
          `var`, `home` etc).
    * [ ] Try to detect the device partition (`/dev/sdb1` etc) based
          on the LUKS2 label.
    * [ ] Cryptdisks script needs fixing as `systemd-tty-ask-password-agent` is not
          catching cryptsetup password prompts anymore. Otherwise, consider to
          deprecate this script.
* [ ] Syncing:
    * [ ] Shall `sync-media` do a `git unannex` on sidecar and other metadata
          files and manage them through Git instead, or keep them as unlocked
          files?
    * [ ] Syncing packages: a frontend to `apt-offline` that uses `git-annex`
          repositories: getting, installing, cleaning. One node can request
          packages through an external drive, and another can fetch then.
    * [ ] Integration with [Baobáxia](https://baobaxia.mocambos.net)?
          Maybe that will already happen if `git-annex` can run directly
          on a mucua's repository.
    * [ ] Merge `sync-home` into `sync-tpc`, or make `sync-tpc` call
          `sync-home`, to avoid code duplication.
* [ ] Provision:
    * [ ] Fix booting issues detailed [here](tpc.md#booting).
* [ ] Upgrade:
    * [ ] Support for [fwupd](https://fwupd.org), at least for showing
          available updates.
    * [ ] Raspbian does not have the `non-free-firmware` component?
          Maybe this is not an issue, as we may move away from
          Raspbian/Raspberry Pi OS.
