# Installation

## Clone

First, clone the code:

    git clone https://git.fluxo.info/hydra

You can use the following to check the integrity of the latest release:

    /usr/bin/git -C hydra verify-commit HEAD

Note that `/usr/bin/git` is called to avoid any other `git` wrappers or aliases
you might have available on your shell.

There are some ways you can install the suite in your system:

* Adding the whole `hydra` folder into your `PATH`.
* By symlinking `hydra`, `hydractl` and optionally `hydras` into your `~/bin` folder.
* By doing a system-wide install at `/usr/local` simply running

        ./hydractl install

## Dependencies

Major direct upstreams:

* `Debian <https://www.debian.org>`_ 
* `Puppet <http://docs.puppetlabs.com>`_
* `Git <https://git-scm.com/>`_
