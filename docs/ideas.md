# Ideas

## Misc

* `hydractl`:
  * `mount-kvmx-supervised`: open encrypted volumes from supervised [kvmx][] guests.
* `hydra`:
  * `newkeys`:
    * generate `luks/root` keys if guest VM.
  * `deploy`:
    * for for tasks / scripted deployment:
      * any language is supported.
      * folder structure like `$HYDRA_FOLDER/tasks/{conf/$task/hosts,stages/{pre,post,main}/$task}`.
      * disable a task script by removing it's file exec flag.
    * run `mount-kvmx-supervised` on remote, ensuring all supervised VMs have their images available
      before applying the puppet catalog.

[kvmx]: https://git.fluxo.info/kvmx/about/
