# Trusted Personal Computers

The Hydra Suite was made with the [Trusted Personal Computer (TPC)][]
([portuguese translation][]) paradigm as it's starting point.

This guide documents the lifecycle of a TPC storage device, or a "TPS
Cartridge":

    Cartridge = TPS = Trusted Personal Storage

A TPS can usually be whatever removable block device like an SSD, microSD, M.2
etc.

The idea is that a number of TPCs can have interchangeable cartridges, as long
as each cartridge has an unique name.

[Trusted Personal Computer (TPC)]: https://web.archive.org/web/20180914153944/http://cmrg.fifthhorseman.net/wiki/TrustedPhysicalConsole
[portuguese translation]: https://plano.autodefesa.org/specs/tpc.html

## Parameters

Start by running the following commands in your terminal, adjusting to your
case:

    export HYDRA="hydra-name"
    export DISK="disk-name"
    export DEVICE="/dev/sdb"
    export DOMAIN="`facter domain`"
    export USER="`whoami`"
    export DEST="/media/$DISK"
    export CONFIG_FOLDER="`hydra $HYDRA folder`"

    if [ "$USER" != "root" ]; then
      export SUDO="sudo"
    fi

## Provisioning a new cartridge

### Random data

If you want to pre-fill your disk with random data, proceed as follows:

    $SUDO dcfldd if=/dev/urandom of=$DEVICE

This can be done in a NAS as it may take days to finish, and the rest of the
procedures from this document can be done from another TPC.

### Configuration

If non-existing, create entries and keys:

    hydra $HYDRA newnode $DISK tpc

Customize:

    vim $CONFIG_FOLDER/config/provision/$DISK.conf
    vim $CONFIG_FOLDER/puppet/config/node/$DISK.$DOMAIN.yaml
    vim $CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN.yaml

### Secrets

Proceed adding other passwords and secrets at
`$CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN`, make sure to
encode them with [hiera-yaml][]:

    hydra $HYDRA eyaml $DISK encrypt -q -o block -p -l some::password

If you already have the secret somewhere, you can use a construction like this
which already adds the eyaml block into the hiera config file:

    keyringer $HYDA decrypt /path/to/some/secret | \
      tr -d '\n' | \
      hydra $HYDRA eyaml $DISK encrypt -q -o block --stdin -l some::password >> \
      $CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN.yaml

You can also ensure a fresh random passphrase is used, using your favourite
generator like this:

    head -c ${1:-20} /dev/urandom | base64 | \
      tr -d '\n' | \
      hydra $HYDRA eyaml $DISK encrypt -q -o block --stdin -l some::random::password >> \
      $CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN.yaml

For `passwd(5)` and `shadow(5)` hashed passphrases, use something like this:

    mkpasswd -m sha-512 | \
      tr -d '\n' | \
      hydra $HYDRA eyaml $DISK encrypt -q -o block --stdin -l some::random::password >> \
      $CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN.yaml

Or this, for a random passphrase:

    head -c ${1:-20} /dev/urandom | base64 | \
      mkpasswd -m sha-512 --stdin | \
      tr -d '\n' | \
      hydra $HYDRA eyaml $DISK encrypt -q -o block --stdin -l some::random::password >> \
      $CONFIG_FOLDER/puppet/config/secrets/node/$DISK.$DOMAIN.yaml

[hiera-yaml]: https://github.com/voxpupuli/hiera-eyaml

### Installation

The proceed with the system installation, which should create all volumes in
the TPS and setup the base Operating System:

    hydra $HYDRA provision $DISK

### Metadata

After installation, make sure to save metadata from the disk layout and LUKS headers, which
can be useful during recovery procedures.

0. Turn off the cartridge, and then plug it again in the TPC you're using. This will make sure
   device information is properly refreshed.

1. Write down UUIDS, models and serials from the new disk. Examples:

        lsblk $DEVICE -n -o +UUID | keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/lsblk
        udevadm info --query=all --name=$DEVICE | keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/udevadm

        # For non-USB and non-microSD disks only
        $SUDO hdparm -I $DEVICE   | keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/hdparm
        $SUDO smartctl -i $DEVICE | keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/smartctl

2. Save partition tables, the LUKS header and other relevant information:

        $SUDO LC_ALL=C sfdisk -d $DEVICE | keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/sfdisk

        $SUDO cryptsetup luksDump /dev/mapper/$DISK-root | keyringer $HYDRA encrypt nodes/houdini.fluxo.info/luksDump-root
        $SUDO cryptsetup luksDump /dev/mapper/$DISK-swap | keyringer $HYDRA encrypt nodes/houdini.fluxo.info/luksDump-swap

        $SUDO cryptsetup luksHeaderBackup /dev/mapper/$DISK-root --header-backup-file luksHeaderBackup-root
        $SUDO chown $(whoami) luksHeaderBackup-root
        keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/luksHeaderBackup-root luksHeaderBackup-root
        chmod +w luksHeaderBackup-root
        wipe luksHeaderBackup-root

## Other security measures

* Optionally, configure ATA passphrases for the new disk. If so, make sure to
  save the passphrase in the keyring:

        keyringer $HYDRA encrypt nodes/$DISK.$DOMAIN/ata

## Mounting

Use the following whenever you need to mount the cartridge in another system:

    hydractl mount-media $DISK

This will make the volume available at `/media/$DISK`.

## Initial user

These steps are needed to setup the encrypted home folder (the second level of encryption)
before continuing the provisioning.

### Setting up home encryption

If you're running this in an existing hydra-compatible TPC but you don't have your
home folder encrypted, backup your data and then proceed running

    hydractl ecryptfs-home

### Syncing the user and encrypted home in the new cartridge

Then, with the new TPS cartridge mounted (`hydractl mount-media $DISK`), do
the following to replicate your user and homedir there:

    $SUDO chroot $DEST adduser $USER
    $SUDO chroot $DEST usermod -a -G sudo $USER
    $SUDO chroot $DEST apt install ecryptfs-utils -y

    $SUDO mkdir -p $DEST/mnt/crypt/home
    $SUDO mv       $DEST/home/$USER           $DEST/mnt/crypt/home/$USER
    $SUDO ln -s -t $DEST/home                 /mnt/crypt/home/$USER/
    $SUDO ln -s -t $DEST/mnt/crypt/home/$USER /home/.ecryptfs/$USER/.ecryptfs
    $SUDO ln -s -t $DEST/mnt/crypt/home/$USER /home/.ecryptfs/$USER/.Private

    $SUDO chroot $DEST chown -R $USER: /mnt/crypt/home/$USER

The procedure may be adapted if you're already running the system from the new TPS
and is logged in there as `root`.

## Syncing data

From another system, and with the cartridge mounted:

    hydractl sync-tpc $DISK

This might involve a lot of data being transmitted.  If you want `sync-tpc` to
ignore all your QEMU virtual machine images hosted at `/var/cache/quemu`, setup
a `.sync-tpc-ignore` like the following:

    $SUDO mkdir -p $DEST/var/cache/qemu
    $SUDO chown $USER: $DEST/var/cache/qemu
    touch $DEST/var/cache/qemu/.sync-tpc-ignore

Check the [backups](backups.md) for additional information.

## Unmounting

Now that basic provisioning is completed, you can unmount the cartridge.

From another system, and with the cartridge mounted:

    hydractl umount-media $DISK

## Booting

The current provisioning procedure (as of 2024-02) may yield into
a partially bootable system, and may need some additional manual
steps right after installation:

    hydractl mount-media $DISK
    sudo chroot $DEST
    update-initramfs -v -u
    hydractl umount-media $DISK

During boot, at the `(initramfs)` prompt, type this to successfully
unlock the encrypted volumes:

    cryptsetup luksOpen /dev/mapper/${DISK}-root root
    cryptsetup luksOpen /dev/mapper/${DISK}-swap swap
    ^Ctrl-D

Then, in the running system, do:

    sudo update-initramfs -v -u

With these steps, the system's boot procedure was fixed.

But there are fixes be done at `hydractl provision` to produce fully working
installations.

## Deploy

Now it's time to boot the new system, log in with your user and deploy the complete
configuration.

The following command will take care of the deployment:

    hydra $HYDRA deploy localhost

## Archives and backups

You can use a hydra-compatible archive/backup disk to setup working copies
for existing archives:

    export                 MEDIA=backup-disk-name
    hydractl mount-media  $MEDIA
    hydractl sync-backups $MEDIA
    hydractl sync-media   $MEDIA

    hydractl sync-media-initremotes localhost boxes hostname-nas1 hostname-nas2

Then add manually other archive remotes.

## Compile

Make sure other nodes have some information on your cartridge by compiling
collected data:

    hydra $HYDRA compile

## Adding a new hardware in the TPC pool

* Proceed with [homologating a new unit](https://plano.autodefesa.org/specs/tpc.html).
* Provision, deploy, sync-media, sync-vms etc.
* Add the devices' network MAC Addresses at `70-persistent-net.rules`, then make sure
  to sync this in the other cartridges.

## Decommissioning

To be documented.

## Additional references

### Implementation

These are implementation-specific references, especially issues, fixes and
workarounds found while developing procedures for TPCs.

#### UEFI

Overview:

* [UEFI boot: how does that actually work, then? | AdamW on Linux and more](https://www.happyassassin.net/posts/2014/01/25/uefi-boot-how-does-that-actually-work-then/ "UEFI boot: how does that actually work, then? | AdamW on Linux and more")
* [Unified Extensible Firmware Interface - ArchWiki](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface "Unified Extensible Firmware Interface - ArchWiki")
* [Linux on UEFI: A Quick Installation Guide](https://www.rodsbooks.com/linux-uefi/ "Linux on UEFI: A Quick Installation Guide")

Coreboot versus UEFI:

* [Coreboot Versus UEFI](https://linuxhint.com/coreboot-vs-uefi/ "Coreboot Versus UEFI")

UEFI versus Legacy:

* [Linux: UEFI vs Legacy IDE. Pros/Cons/Security? : linuxquestions](https://www.reddit.com/r/linuxquestions/comments/6kw648/linux_uefi_vs_legacy_ide_prosconssecurity/ "Linux: UEFI vs Legacy IDE. Pros/Cons/Security? : linuxquestions")

Lenovo:

* [lenovo - UEFI or Legacy? Which is advised and why? - Ask Ubuntu](https://askubuntu.com/questions/647303/uefi-or-legacy-which-is-advised-and-why "lenovo - UEFI or Legacy? Which is advised and why? - Ask Ubuntu")
* [Legacy BIOS boot support removed in Lenovo's 2020 products](https://support.lenovo.com/br/en/solutions/ht510878-legacy-bios-boot-support-removed-in-lenovo-2020-products "Access Denied")

Issues:

* [UEFI troubleshooting | Qubes OS](https://www.qubes-os.org/doc/uefi-troubleshooting/ "UEFI troubleshooting | Qubes OS")

#### Boot loader

Geral:

* [GNU GRUB Manual 2.06: Installing GRUB using grub-install](https://www.gnu.org/software/grub/manual/grub/html_node/Installing-GRUB-using-grub_002dinstall.html#Installing-GRUB-using-grub_002dinstall "GNU GRUB Manual 2.06: Installing GRUB using grub-install")
* [GRUB - ArchWiki](https://wiki.archlinux.org/title/GRUB#Default/fallback_boot_path "GRUB - ArchWiki")
* [GRUB/Tips and tricks - ArchWiki](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#UEFI_further_reading "GRUB/Tips and tricks - ArchWiki")

(Re)installation:

* [GrubEFIReinstall - Debian Wiki](https://wiki.debian.org/GrubEFIReinstall "GrubEFIReinstall - Debian Wiki")
* [Chapter 13. Encrypting block devices using LUKS Red Hat Enterprise Linux 8 | Red Hat Customer Portal](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/encrypting-block-devices-using-luks_security-hardening)

Full Disk Encryption (FDE) support:

* [How can I install Debian with full-disk encryption and a custom sized swapfile? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/577379/how-can-i-install-debian-with-full-disk-encryption-and-a-custom-sized-swapfile "How can I install Debian with full-disk encryption and a custom sized swapfile? - Unix & Linux Stack Exchange")

Recovery:

* [GRUB Recovery for UEFI-GPT-LUKS-LVM](https://arnaudr.io/2018/03/26/grub-recovery-for-uefi-gpt-luks-lvm/ "Arnaud R")
* [Re: Does grub-mkrescue support UEFI hybrid USB/DVD images?](https://lists.gnu.org/archive/html/grub-devel/2015-01/msg00042.html "Re: Does grub-mkrescue support UEFI hybrid USB/DVD images?")

Issues:

* [grub2 - UEFI Grub fails to boot encrypted Ubuntu 21.04 system: can't find command cryptomoun - Ask Ubuntu](https://askubuntu.com/questions/1351914/uefi-grub-fails-to-boot-encrypted-ubuntu-21-04-system-cant-find-command-crypt)
  * [PSA: GRUB fails to detect the fact that / and /boot are LUKS encrypted : archlinux](https://www.reddit.com/r/archlinux/comments/o1opyj/psa_grub_fails_to_detect_the_fact_that_and_boot/ "PSA: GRUB fails to detect the fact that / and /boot are LUKS encrypted : archlinux")
  * [grub2 - How do I get Grub to automatically run cryptomount to load its config file (encrypted boot) - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/313939/how-do-i-get-grub-to-automatically-run-cryptomount-to-load-its-config-file-encr "grub2 - How do I get Grub to automatically run cryptomount to load its config file (encrypted boot) - Unix & Linux Stack Exchange")
  * [#917117 - grub-efi-amd64-signed: doesn't mount cryptodisk - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=917117 "#917117 - grub-efi-amd64-signed: doesn't mount cryptodisk - Debian Bug report logs")
  * [GRUB + LUKS2 not asking for password](https://forum.artixlinux.org/index.php/topic,2093.0.html "GRUB + LUKS2 not asking for password")
* [FS#40029 : [grub] ${cmdpath} expansion in GRUB shell is wrong](https://bugs.archlinux.org/task/40029 "FS#40029 : [grub] ${cmdpath} expansion in GRUB shell is wrong")
  * [linux - How to reconfigure grub - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/577780/how-to-reconfigure-grub "linux - How to reconfigure grub - Unix & Linux Stack Exchange")
  * [grub2 - wrong grub prefix (efi) `grub-install` and `update-grub` doesn't fix it - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/615109/wrong-grub-prefix-efi-grub-install-and-update-grub-doesnt-fix-it "grub2 - wrong grub prefix (efi) `grub-install` and `update-grub` doesn't fix it - Unix & Linux Stack Exchange")
  * [#925309 - Wrong prefix directory hardcoded in signed GRUB image - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=925309 "#925309 - Wrong prefix directory hardcoded in signed GRUB image - Debian Bug report logs")
  * [grub2 - grub doesn't load grub.cfg automatically - Ask Ubuntu](https://askubuntu.com/questions/885445/grub-doesnt-load-grub-cfg-automatically "grub2 - grub doesn't load grub.cfg automatically - Ask Ubuntu")
    * [ubuntu - Grub not loading config file or booting into linux automatically - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/232730/grub-not-loading-config-file-or-booting-into-linux-automatically#247716 "ubuntu - Grub not loading config file or booting into linux automatically - Unix & Linux Stack Exchange")

#### SecureBoot

* [SecureBoot - Debian Wiki](https://wiki.debian.org/SecureBoot "SecureBoot - Debian Wiki")
  * [DEBAMAX — Debian expertise — Blog — An overview of Secure Boot in Debian](https://debamax.com/blog/2019/04/19/an-overview-of-secure-boot-in-debian/ "DEBAMAX — Debian expertise — Blog — An overview of Secure Boot in Debian")
  * [Debian -- GRUB2 UEFI SecureBoot vulnerabilities - 2021](https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot/ "Debian -- GRUB2 UEFI SecureBoot vulnerabilities - 2021")
  * [SecureBoot/Discussion - Debian Wiki](https://wiki.debian.org/SecureBoot/Discussion "SecureBoot/Discussion - Debian Wiki")
  * [SecureBoot/VirtualMachine - Debian Wiki](https://wiki.debian.org/SecureBoot/VirtualMachine "SecureBoot/VirtualMachine - Debian Wiki")
  * [Frequently asked questions (FAQ) | Qubes OS](https://www.qubes-os.org/faq/#is-secure-boot-supported "Frequently asked questions (FAQ) | Qubes OS")
    * [Heads - Trammell Hudson's Projects](https://trmm.net/Heads/ "Heads - Trammell Hudson's Projects")
    * [About - Heads - Wiki](https://osresearch.net/ "About - Heads - Wiki")
  * [GitHub - osresearch/safeboot: Scripts to slightly improve the security of the Linux boot process with UEFI Secure Boot and TPM support](https://github.com/osresearch/safeboot "GitHub - osresearch/safeboot: Scripts to slightly improve the security of the Linux boot process with UEFI Secure Boot and TPM support")
    * [Xen / Qubes support · Issue #21 · osresearch/safeboot · GitHub](https://github.com/osresearch/safeboot/issues/21 "Xen / Qubes support · Issue #21 · osresearch/safeboot · GitHub")
  * [Debian-GNU-Linux-Profiles/build-secureboot-trustchain.md at master · hardenedlinux/Debian-GNU-Linux-Profiles · GitHub](https://github.com/hardenedlinux/Debian-GNU-Linux-Profiles/blob/master/docs/hardened_boot/build-secureboot-trustchain.md "Debian-GNU-Linux-Profiles/build-secureboot-trustchain.md at master · hardenedlinux/Debian-GNU-Linux-Profiles · GitHub")
  * [Secure Boot with GRUB 2 and signed Linux images and initrds](https://ruderich.org/simon/notes/secure-boot-with-grub-and-signed-linux-and-initrd "Secure Boot with GRUB 2 and signed Linux images and initrds")

#### Evil Maid Attack

* [Evil maid attack - Wikipedia](https://en.wikipedia.org/wiki/Evil_maid_attack "Evil maid attack - Wikipedia")
  * [Anti-Evil Maid with UEFI and Xen - Brendan Kerrigan, Assured Information Security - Platform Security Summit 2018](https://www.platformsecuritysummit.com/2018/speaker/kerrigan/ "Anti-Evil Maid with UEFI and Xen - Brendan Kerrigan, Assured Information Security - Platform Security Summit 2018")
    * [PSEC2018-Anti-Evil-Maid-UEFI-Xen-Brendan-Kerrigan.pdf](https://www.platformsecuritysummit.com/2018/speaker/kerrigan/PSEC2018-Anti-Evil-Maid-UEFI-Xen-Brendan-Kerrigan.pdf "PSEC2018-Anti-Evil-Maid-UEFI-Xen-Brendan-Kerrigan.pdf")
  * [F-Secure Whitepaper - Evil Maid Guide (English).pdf](https://www.f-secure.com/content/dam/press/de/media-library/reports/F-Secure%20Whitepaper%20-%20Evil%20Maid%20Guide%20(English).pdf "F-Secure Whitepaper - Evil Maid Guide (English).pdf")
  * [UEFI threats moving to the ESP: Introducing ESPecter bootkit | WeLiveSecurity](https://www.welivesecurity.com/2021/10/05/uefi-threats-moving-esp-introducing-especter-bootkit/ "UEFI threats moving to the ESP: Introducing ESPecter bootkit | WeLiveSecurity")
  * [disk encryption - Encrypting the /boot partition in a Linux system can protect from an Evil Maid Attack? - Information Security Stack Exchange](https://security.stackexchange.com/questions/166075/encrypting-the-boot-partition-in-a-linux-system-can-protect-from-an-evil-maid-a "disk encryption - Encrypting the /boot partition in a Linux system can protect from an Evil Maid Attack? - Information Security Stack Exchange")
    * [Ten Immutable Laws Of Security (Version 2.0) | Microsoft Docs](https://docs.microsoft.com/en-us/archive/blogs/rhalbheer/ten-immutable-laws-of-security-version-2-0 "Ten Immutable Laws Of Security (Version 2.0) | Microsoft Docs")
  * [seagl-2017.pdf](https://ia600805.us.archive.org/7/items/seagl-2017/seagl-2017.pdf "seagl-2017.pdf")
  * [GitHub - kritjo/Techniques-for-Secure-System-Boot: Paper for mid-semester exam in IN2120 at Institute for Informathics, University of Oslo. Ended up scoring 98,8%.](https://github.com/kritjo/Techniques-for-Secure-System-Boot "GitHub - kritjo/Techniques-for-Secure-System-Boot: Paper for mid-semester exam in IN2120 at Institute for Informathics, University of Oslo. Ended up scoring 98,8%.")
  * [GitHub - xmikos/cryptboot: Encrypted boot partition manager with UEFI Secure Boot support](https://github.com/xmikos/cryptboot "GitHub - xmikos/cryptboot: Encrypted boot partition manager with UEFI Secure Boot support")
  * [Arch Linux Full-Disk Encryption Installation Guide [Encrypted Boot, UEFI, NVMe, Evil Maid] · GitHub](https://gist.github.com/m-bartlett/3ff47b31947cd1e504268415c8054f50 "Arch Linux Full-Disk Encryption Installation Guide [Encrypted Boot, UEFI, NVMe, Evil Maid] · GitHub")
  * [GitHub - QubesOS/qubes-antievilmaid: Qubes component: antievilmaid](https://github.com/QubesOS/qubes-antievilmaid "GitHub - QubesOS/qubes-antievilmaid: Qubes component: antievilmaid")
  * [Secure Boot, TPM2, LUKS, and a potential security hole in the Wiki / GNU/Linux Discussion / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=271423 "Secure Boot, TPM2, LUKS, and a potential security hole in the Wiki / GNU/Linux Discussion / Arch Linux Forums")
  * [luks - Defending against the evil maid, how to handle removal of the /boot partition - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/189412/defending-against-the-evil-maid-how-to-handle-removal-of-the-boot-partition "luks - Defending against the evil maid, how to handle removal of the /boot partition - Unix & Linux Stack Exchange")
  * [Multiple encryption - Wikipedia](https://en.wikipedia.org/wiki/Multiple_encryption "Multiple encryption - Wikipedia")

#### Howtos

* [Creating a BIOS/GPT and UEFI/GPT Grub-bootable Linux system - Philipp's Tech Blog](https://blog.heckel.io/2017/05/28/creating-a-bios-gpt-and-uefi-gpt-grub-bootable-linux-system/ "Creating a BIOS/GPT and UEFI/GPT Grub-bootable Linux system - Philipp's Tech Blog")
  * [How to Install GRUB2 with EFI Support | Timesys LinuxLink](https://linuxlink.timesys.com/docs/engineering/wiki/HOWTO_Install_GRUB2_with_EFI_support "How to Install GRUB2 with EFI Support | Timesys LinuxLink")
  * [Real full disk encryption using GRUB on Arch Linux for BIOS and UEFI](https://unixsheikh.com/tutorials/real-full-disk-encryption-using-grub-on-arch-linux-for-bios-and-uefi.html "Real full disk encryption using GRUB on Arch Linux for BIOS and UEFI")
  * [Real full disk encryption using GRUB on Debian GNU/Linux for BIOS](https://unixsheikh.com/tutorials/real-full-disk-encryption-using-grub-on-debian-linux-for-bios.html "Real full disk encryption using GRUB on Debian GNU/Linux for BIOS")
  * [Real full disk encryption using GRUB on Void Linux for BIOS](https://unixsheikh.com/tutorials/real-full-disk-encryption-using-grub-on-void-linux-for-bios.html "Real full disk encryption using GRUB on Void Linux for BIOS")
  * [Install Debian with Debootstrap + Grub EFI · GitHub](https://gist.github.com/superboum/1c7adcd967d3e15dfbd30d04b9ae6144 "Install Debian with Debootstrap + Grub EFI · GitHub")
  * [Debian: Debootstrap Install (aka Installing Debian “the Arch Way”) – LeCorbeau's Vault](https://lecorbeausvault.wordpress.com/2021/10/07/debian-debootstrap-install-aka-installing-debian-the-arch-way/ "Debian: Debootstrap Install (aka Installing Debian “the Arch Way”) – LeCorbeau's Vault")
    * [Debian -- Details of package arch-install-scripts in bullseye](https://packages.debian.org/bullseye/arch-install-scripts "Debian -- Details of package arch-install-scripts in bullseye")
  * [Instructions how to install Debian using debootstrap · GitHub](https://gist.github.com/varqox/42e213b6b2dde2b636ef "Instructions how to install Debian using debootstrap · GitHub")

#### TRIM

* [Trim (computing) - Wikipedia](https://en.wikipedia.org/wiki/Trim_(computing) "Trim (computing) - Wikipedia")
  * [fstrim.pdf](https://people.redhat.com/mskinner/rhug/q1.2014/fstrim.pdf "fstrim.pdf")
  * [Enable periodic TRIM - including on a LUKS partition - Tech Knowledge Base - jaytaala.com Confluence](https://confluence.jaytaala.com/display/TKB/Enable+periodic+TRIM+-+including+on+a+LUKS+partition "Enable periodic TRIM - including on a LUKS partition - Tech Knowledge Base - jaytaala.com Confluence")
  * [How to properly activate TRIM for your SSD on Linux: fstrim, lvm and dm-crypt | synaptic fault](http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/ "How to properly activate TRIM for your SSD on Linux: fstrim, lvm and dm-crypt | synaptic fault")
  * [ssd - fstrim doesn't seem to trim a partition that uses lvm and dm-crypt - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/346195/fstrim-doesnt-seem-to-trim-a-partition-that-uses-lvm-and-dm-crypt#348016 "ssd - fstrim doesn't seem to trim a partition that uses lvm and dm-crypt - Unix & Linux Stack Exchange")
  * [ssd - Trim with LVM and dm-crypt - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/85865/trim-with-lvm-and-dm-crypt "ssd - Trim with LVM and dm-crypt - Unix & Linux Stack Exchange")
  * [[SOLVED] fstrim with LVM on LUKS: discard operation not supported / Installation / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=231086 "[SOLVED] fstrim with LVM on LUKS: discard operation not supported / Installation / Arch Linux Forums")
  * [Linux/LUKS/Full Disk Encryption: How can I mitigate SSD/flash media security security risk caused by unreliable physical deletion? - Information Security Stack Exchange](https://security.stackexchange.com/questions/248917/linux-luks-full-disk-encryption-how-can-i-mitigate-ssd-flash-media-security-sec "Linux/LUKS/Full Disk Encryption: How can I mitigate SSD/flash media security security risk caused by unreliable physical deletion? - Information Security Stack Exchange")
  * [linux - How to enable discards on encrypted root - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/574400/how-to-enable-discards-on-encrypted-root "linux - How to enable discards on encrypted root - Unix & Linux Stack Exchange")
  * [SSD - Gentoo Wiki](https://wiki.gentoo.org/wiki/SSD#Discard_.28trim.29_support "SSD - Gentoo Wiki")
    * [ssd - What exactly do the 'allow-discards' and 'root_trim' Linux parameters do? - Ask Ubuntu](https://askubuntu.com/questions/243518/what-exactly-do-the-allow-discards-and-root-trim-linux-parameters-do "ssd - What exactly do the 'allow-discards' and 'root_trim' Linux parameters do? - Ask Ubuntu")
  * [Solid state drive - ArchWiki](https://wiki.archlinux.org/title/Solid_state_drive "Solid state drive - ArchWiki")
  * [TRIM on LVM on LUKS on SSD – Just another Linux geek](https://blog.christophersmart.com/2013/06/05/trim-on-lvm-on-luks-on-ssd/ "TRIM on LVM on LUKS on SSD – Just another Linux geek")
  * [Possible to get SSD TRIM (discard) working on ext4 + LVM + software RAID in Linux? - Server Fault](https://serverfault.com/questions/227918/possible-to-get-ssd-trim-discard-working-on-ext4-lvm-software-raid-in-linu "Possible to get SSD TRIM (discard) working on ext4 + LVM + software RAID in Linux? - Server Fault")
  * [Discard (TRIM) with KVM Virtual Machines... in 2020! - Chris Irwin's Blog](https://chrisirwin.ca/posts/discard-with-kvm-2020/ "Discard (TRIM) with KVM Virtual Machines... in 2020! - Chris Irwin's Blog")
  * [Implementing Linux fstrim on SSD with software md-raid - Server Fault](https://serverfault.com/questions/508459/implementing-linux-fstrim-on-ssd-with-software-md-raid#508463 "Implementing Linux fstrim on SSD with software md-raid - Server Fault")
    * [Implementing Linux fstrim on SSD with software md-raid - Ringing Liberty](https://www.ringingliberty.com/2013/05/16/implementing-linux-fstrim-on-ssd-with-software-md-raid/ "Implementing Linux fstrim on SSD with software md-raid - Ringing Liberty")
    * [ubuntu - Why does fstrim trim all of the free space on my mdraid mirror after a reboot? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/124293/why-does-fstrim-trim-all-of-the-free-space-on-my-mdraid-mirror-after-a-reboot "ubuntu - Why does fstrim trim all of the free space on my mdraid mirror after a reboot? - Unix & Linux Stack Exchange")
    * [How to set up SSD raid and TRIM support? - Ask Ubuntu](https://askubuntu.com/questions/264625/how-to-set-up-ssd-raid-and-trim-support "How to set up SSD raid and TRIM support? - Ask Ubuntu")
  * [Debian, QEMU, libvirt, qcow2 and fstrim – #Jan](https://jschumacher.info/2020/11/debian-qemu-libvirt-qcow2-and-fstrim/ "Debian, QEMU, libvirt, qcow2 and fstrim – #Jan")
    * [Discard (TRIM) with KVM Virtual Machines... in 2020! - Chris Irwin's Blog](https://chrisirwin.ca/posts/discard-with-kvm-2020/ "Discard (TRIM) with KVM Virtual Machines... in 2020! - Chris Irwin's Blog")
      * [Discard (TRIM) with KVM Virtual Machines - Chris Irwin's Blog](https://chrisirwin.ca/posts/discard-with-kvm/ "Discard (TRIM) with KVM Virtual Machines - Chris Irwin's Blog")
    * [TRIM Support on KVM Virtual Machines – ZenCoffee Blog – random notes, guides, and thoughts…](http://blog.zencoffee.org/2016/05/trim-support-kvm-virtual-machines/ "TRIM Support on KVM Virtual Machines – ZenCoffee Blog – random notes, guides, and thoughts…")
    * [#763318 - qemu-kvm: discard='unmap' not working for qcow2 disks on virtio-scsi controller - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=763318 "#763318 - qemu-kvm: discard='unmap' not working for qcow2 disks on virtio-scsi controller - Debian Bug report logs")
    * [linux - Qemu TRIM and discard on a physical SSD device - Server Fault](https://serverfault.com/questions/896448/qemu-trim-and-discard-on-a-physical-ssd-device "linux - Qemu TRIM and discard on a physical SSD device - Server Fault")
  * [Milan Broz's blog: TRIM & dm-crypt ... problems?](https://asalor.blogspot.com/2011/08/trim-dm-crypt-problems.html "Milan Broz's blog: TRIM & dm-crypt ... problems?")
    * [#11870 (Add solid state drives to some servers) – Support](https://support.mayfirst.org/ticket/11870 "#11870 (Add solid state drives to some servers) – Support")

#### Swap

* [Swap (and hibernation) on SSD in 2019?](https://askubuntu.com/questions/1165507/swap-and-hibernation-on-ssd-in-2019#1165544)
  * [Why are swap partitions discouraged on SSD drives, are they harmful? - Ask Ubuntu](https://askubuntu.com/questions/652337/why-are-swap-partitions-discouraged-on-ssd-drives-are-they-harmful?noredirect=1&lq=1 "Why are swap partitions discouraged on SSD drives, are they harmful? - Ask Ubuntu")
  * [Swap (and hibernation) on SSD in 2019? - Ask Ubuntu](https://askubuntu.com/questions/1165507/swap-and-hibernation-on-ssd-in-2019 "Swap (and hibernation) on SSD in 2019? - Ask Ubuntu")
  * [Swap and SSD linux - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/589598/swap-and-ssd-linux "Swap and SSD linux - Unix & Linux Stack Exchange")

#### Cryptsetup

* [Debian Cryptsetup docs – README](https://cryptsetup-team.pages.debian.net/cryptsetup/README.initramfs.html "Debian Cryptsetup docs – README")
  * [partitioning - Should I use LUKS1 or LUKS2 for partition encryption? - Ask Ubuntu](https://askubuntu.com/questions/1032546/should-i-use-luks1-or-luks2-for-partition-encryption "partitioning - Should I use LUKS1 or LUKS2 for partition encryption? - Ask Ubuntu")
    * [devconf2016-luks2.pdf](https://mbroz.fedorapeople.org/talks/DevConf2016/devconf2016-luks2.pdf "devconf2016-luks2.pdf")
    * [Cracking LUKS/dm-crypt passphrases – Diverto – Information Security Warriors](https://diverto.github.io/2019/11/18/Cracking-LUKS-passphrases "Cracking LUKS/dm-crypt passphrases – Diverto – Information Security Warriors")
  * [#671037 - can not change target name of root in /etc/crypttab - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=671037 "#671037 - can not change target name of root in /etc/crypttab - Debian Bug report logs")
  * [Debian -- Details of package cryptsetup-initramfs in bullseye](https://packages.debian.org/bullseye/cryptsetup-initramfs "Debian -- Details of package cryptsetup-initramfs in bullseye")

#### Certification

* [Certified hardware | Qubes OS](https://www.qubes-os.org/doc/certified-hardware/ "Certified hardware | Qubes OS")
