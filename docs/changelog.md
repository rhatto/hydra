# ChangeLog

## 0.3.0 - Unrelased

### hydra

* [x] Deploy:
    * [x] Automatically fix permission of (or delete) puppet's `devices`
          folder: https://github.com/puppetlabs/puppet-specifications/blob/master/file_paths.md
* [x] Keys:
    * [x] Deprecate generating and deploying/import borg keys, since
          pre-generation is not a supported behavior right now:
          https://github.com/borgbackup/borg/issues/7047
    * [x] Document about how keys are encrypted and backed up in the server:
          https://borgbackup.readthedocs.io/en/latest/faq.html#how-important-is-the-home-config-borg-directory

### hydractl

* [x] Provision: increase default partition sizes from 20G to 40G, as nowadays
      distro size increase a lot and a 20G system partition can be filled in
      easily.
