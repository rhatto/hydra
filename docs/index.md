# Hydra Suite - Command and Control

![Hydra](assets/logo.png){ align=left }

Hydra is a suite for orchestration and management of machines composed of
**lots of conventions and assumptions**, and the following basic commands:

* `hydractl`: act in the current host.
* `hydra`:    act on a set of hosts.
* `hydras`:   act on a superset of hosts.

An Hydra is not a "cloud computing" platform, it's something else: a set of
systems that operate together, where any node can be used to spawn new nodes.

This is an ongoing experiment in how a person or a collective can manage
many computers in an unified way. It's not production ready, and it always
struggles to pass the test of time.

Running the Hydra Suite is not recommended at this point, except if you
want to contribute with it's development.

But studying it's documentation, code, conventions is concepts is highly
encouraged, as it may give you some ideas to manage your own systems.

Check also the [concept presentation](slides) (portuguese only).
